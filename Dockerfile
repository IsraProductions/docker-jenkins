FROM jenkins/jenkins:alpine

ENV JAVA_OPTS="-Djenkins.install.runSetupWizard=false"

ENV JENKINS_USER admin
ENV JENKINS_PASS admin

COPY config/ /usr/share/jenkins/ref/

COPY plugins.txt .
RUN /usr/local/bin/install-plugins.sh < plugins.txt
